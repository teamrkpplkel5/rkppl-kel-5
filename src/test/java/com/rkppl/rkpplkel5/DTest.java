package com.rkppl.rkpplkel5;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author fahrizalnuansa
 */
public class DTest extends D{
    
    // Method ini berjalan sebelum test case di jalankan
    @Before
    public void awalTest(){
	System.out.println("Mengawali Testing");
    }

    
//     Test case untuk fungsi untuk pangkat bilangan, karna menggunakan assertNotEquals jadi jika expected sama dengan actual
//     maka hasilnya false, jika berbeda maka true
    
    @Test
    public void testPow1(){
	assertNotEquals("seharusnya 8", 4.0, pow(2, 3), 0.0);
    }

    /* 
     * Test case untuk fungsi untuk pangkat bilangan, karna menggunakan assertEquals jadi jika expected sama dengan actual
     * maka hasilnya true, jika berbeda maka false
    */
    @Test
    public void testPow2(){
	assertEquals("seharusnya 8", 9.0, pow(2, 3), 0.0);
    }
    
    /* 
     * Test case untuk fungsi pembagian, karna menggunakan assertNotEquals jadi jika expected sama dengan actual
     * maka hasilnya false, jika berbeda maka true
    */
    @Test
    public void testMul1(){
	assertNotEquals("seharusnya 6", 4.0, mul(2, 3), 0.0);
    }
    
    /* 
     * Test case untuk fungsi perkalian, karna menggunakan assertEquals jadi jika expected sama dengan actual
     * maka hasilnya true, jika berbeda maka false
    */
    @Test
    public void testMul2(){
	assertEquals("seharusnya 6", 6.0, mul(2, 3), 0.0);
    }
    
    /* 
     * Test case untuk fungsi perkalian, karna menggunakan assertNotEquals jadi jika expected sama dengan actual
     * maka hasilnya false, jika berbeda maka true
    */
    @Test
    public void testDiv1(){
	assertNotEquals("seharusnya 2", 4.0, div(6, 3), 0.0);
    }
    
    /* 
     * Test case untuk fungsi pembagian, karna menggunakan assertEquals jadi jika expected sama dengan actual
     * maka hasilnya true, jika berbeda maka false
    */
    @Test
    public void testDiv2(){
	assertEquals("seharusnya 2", 2.0, div(6, 3), 0.0);
    }
    
    // Method ini berjalan sesudah test case di jalankan
    @After
    public void akhirTest(){
	System.out.println("Akhir testing");
    }
}
