/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rkppl.rkpplkel5;

import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Septian Candra K
 */

//kelas untuk mengetes kelas C sehingga bisa mengetahui kesalahan serta letak code yang salahnya
public class CTest {
    
    private static C c;

    //awal pengetesan untuk beberapa method
    @Before
    public void awalTest() {
        c = new C();
        System.out.println("Mengawali Testing");
    }

    //method test untuk pertambahan
    @Test
    public void testAdd1() {
        assertNotEquals("seharusnya 5", 4, c.add(3, 2));
    }

    @Test
    public void testAdd2() {
        assertEquals("seharusnya 5", 5, c.add(2, 3));
    }
    //akhir metthod test pertambahan
    
    //method test pengurangan
    @Test
    public void testSub1() {
        assertNotEquals("seharusnya 1", 3, c.sub(4, 3));
    }

    @Test
    public void testSub2() {
        assertEquals("seharusnya 3", 3, c.sub(6, 3));
    }
    //akhir pengetesan pengurangan
    
    //method test perkalian
    @Test
    public void testMul1() {
        assertNotEquals("seharusnya 18", 13, c.mul(6, 3));
    }

    @Test
    public void testMul2() {
        assertEquals("seharusnya 18", 18, c.mul(6, 3));
    }
    //akhir method pengetesan perkalian
    
    //method test pembagian
    @Test
    public void testDiv1(){
        assertEquals("seharusnya 2", 2, c.div(6, 3));
    }
    
    @Test
    public void testDiv2(){
        assertNotEquals("seharusnya 2", 9, c.div(6, 3));
    }
    //akhir pengetsan pembagian
    
    //setelah dilakukan pengetesan
    @After
    public void akhirTest() {
        System.out.println("Akhir testing");
    }
}
