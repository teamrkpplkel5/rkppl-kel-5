package com.rkppl.rkpplkel5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author fahrizalnuansa dan Septian Candra K
 */
public class MainApp 
{
    public static void main( String[] args )
    {
        String[] arr = {"Fahrizal Nuansa", "Septian"};
        
        E e = new E();  
        List<String> wordList = Arrays.asList(arr);  
        e.setListDataE(wordList);
        
        D d = new D();
        d.pow(2, 3);
        d.mul(2, 3);
        d.div(6, 3);
        
        C c = new C();
        
        c.add(3, 2);
        c.sub(4, 3);
        c.mul(6, 3);
        c.div(6, 3);
    }
}
