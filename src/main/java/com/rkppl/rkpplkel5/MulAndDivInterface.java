
package com.rkppl.rkpplkel5;

/**
 *
 * @author fahrizalnuansa
 */
public interface MulAndDivInterface {
    
    //Interface untuk fungsi perkalian
    public double mul(double n1,double n2 );
    
    //Interface untuk fungsi pembagian dengan membagi dua buah bilangan
    public double div(double n1, double n2);
    
}
