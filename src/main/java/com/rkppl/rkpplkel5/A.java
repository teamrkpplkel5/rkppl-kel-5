package com.rkppl.rkpplkel5;

/**
 *
 * @author Septian Candra K
 */

//kelas ini menggunakan kelas interface yaitu kelas AddANdSubinterface
public class A {
    
    // konstruktor kelas A
    public A (){
        
    }
    // method string to String berisi return toString 
    @Override
    public String toString() {
        return "A{" + '}';
    }   
}
