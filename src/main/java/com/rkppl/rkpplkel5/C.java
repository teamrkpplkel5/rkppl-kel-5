package com.rkppl.rkpplkel5;

/**
 *
 * @author Septian Candra K
 */
public class C extends Math {
    double n1;
    double n2;
    
    A a = new A();
    B b = new B();
    //konstrutor kelas C
    public C(){
        
    }
    
    //method pertambahan variabel n1+n2
    public int add(int n1, int n2){
       return (int) (n1 + n2);
    }
    
    //method pengurangan variabel n1-n2
    public int sub(int n1, int n2){
       return (int) (n1 - n2);
    }
    
    //method perkalian variabel n1*n2
    public int mul(int n1, int n2){
       return (int) (n1 * n2);
    }
    
    //method pembagian variabel n1/n2
    public int div(int n1, int n2){
       return (int) (n1 / n2);
    }
    
    //method toSting dan mengembalikan varibel n1 dan n2 dan nilai yang dikembalikan bertipe data string
    @Override
    public String toString(){
       return "C{" + "n1=" + n1 + ", n2=" + n2 + '}';
    } 
}
