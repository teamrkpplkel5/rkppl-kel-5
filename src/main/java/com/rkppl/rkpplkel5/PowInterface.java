
package com.rkppl.rkpplkel5;

/**
 *
 * @author fahrizalnuansa
 */
public interface PowInterface extends MulAndDivInterface{
    
    //Interface untuk fungsi pangkat bilangan 
    public double pow (double n1, double n2);

}
