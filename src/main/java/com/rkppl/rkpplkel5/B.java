package com.rkppl.rkpplkel5;

/**
 *
 * @author fahrizalnuansa
 */
public class B implements MulAndDivInterface{
    
    //Konstruktor untuk kelas B, dan menampilkan "Ctor di kelas D"
    public B() {
        System.out.println("Ctor di kelas B");
        System.out.println(toString());
    }

    //method toString() digunakan untuk representasi object kedalam string, dalam kelas ini akan menampilkan "toString di kelas B"
    @Override
    public String toString() {
        return "toString di kelas B";
    }
    
    // method di override dari interface MulAndDivInterface atau kelas perkalian dan pembagian
    @Override
    public double mul(double n1, double n2) {
        return n1*n2;
    }

    // method di override dari interface MulAndDivInterface
    @Override
    public double div(double n1, double n2) {
        return n1/n2;
    }
    
}
