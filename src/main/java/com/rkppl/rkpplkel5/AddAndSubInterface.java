package com.rkppl.rkpplkel5;

/**
 *
 * @author Septian Candra K
 */

//Kelas interface ini digunakan oleh kelas A

public interface AddAndSubInterface {

    public int add(int n1, int n2);

    public int sub(int n1, int n2);
}