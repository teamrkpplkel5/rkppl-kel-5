package com.rkppl.rkpplkel5;

/**
 *
 * @author fahrizalnuansa
 */
public class D implements PowInterface{
    
    //Konstruktor untuk kelas D, dan menampilkan "Ctor di kelas D" X
    public D(){
        System.out.println("Ctor di kelas D");
        System.out.println(toString());
    }

    //method toString() digunakan untuk representasi object kedalam string, dalam kelas ini akan menampilkan "toString di kelas D"
    @Override
    public String toString() {
        return "toString di kelas D";
    }

    // method di override dari interface PowInterface
    @Override
    public double pow(double n1, double n2) {
        int hasil = 1;
        int n = 1;
        while(n <= n2){
            n++;
            hasil *= n1;
        }
        return hasil;
    }

    // method di override dari interface MulAndDivInterface
    @Override
    public double mul(double n1, double n2) {
        return n1*n2;
    }

    // method di override dari interface MulAndDivInterface
    @Override
    public double div(double n1, double n2) {
        return n1/n2;
    }
    
}
