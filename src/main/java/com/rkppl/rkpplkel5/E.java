package com.rkppl.rkpplkel5;



import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fahrizalnuansa
 */
public class E {
    
    List listDataE = new ArrayList<String[]>();
    
    //Konstruktor untuk kelas E, dan menampilkan "Ctor di kelas E"
    public E(){
        System.out.println("Ctor di kelas E");
        System.out.println(toString());
    }
    
    //Method ini berfungsi untuk mengambil seluruh data yang ada pada listDataE
    public List getListDataE() {
        return listDataE;
    }

    //Method ini berfungsi untuk menyimpan data pada listDataE
    public void setListDataE(List listDataE) {
        this.listDataE = listDataE;
    }

    //method toString() digunakan untuk representasi object kedalam string, dalam kelas ini akan menampilkan "toString di kelas E"
    @Override
    public String toString() {
        return "toString di kelas E";
    }
    
}
