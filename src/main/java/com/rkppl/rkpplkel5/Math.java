package com.rkppl.rkpplkel5;

/**
 *
 * @author Septian Candra K
 */

//kelas ini memiliki kesamaan fungsi dengan kelas C sehingga dinamakan generalization
public abstract class Math {
    public int mod(int n1, int n2) {
        return n1 % n2;
    }

    public double sqrt(double n) {
        return 0;
    }
}
